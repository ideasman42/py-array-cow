
import unittest
import os

import array_cow

if "__pypy__" not in __import__("sys").builtin_module_names:
    print("\nWARNING: these tests run very slow in CPython, best use PyPy3x\n")
    __import__("sys").setrecursionlimit(2000)


def repr_stats(bs):
    size_real = array_cow.BLI_array_store_state_size_real_calc(bs)
    size_full = array_cow.BLI_array_store_state_size_expanded_calc(bs)
    percent = (size_real / size_full) * 100 if size_full else 0
    return ("%d, %d, (%d) %.8f%%" %
            (size_real, size_full, size_full - size_real, percent))


def array_cow_from_bytes(
        data_list,
        *,
        stride, chunk_count):
    """
    Return a new array_cow from a list of data.
    """

    bs = array_cow.BLI_array_store_create(stride=stride, chunk_count=chunk_count)
    state_parent = None

    states = []

    for data in data_list:
        state_parent = array_cow.BLI_array_store_state_add(bs, data, state_parent)
        states.append(state_parent)
    return bs, states


def array_cow_size_info(bs):
    return (array_cow.BLI_array_store_state_size_real_calc(bs),
            array_cow.BLI_array_store_state_size_expanded_calc(bs))


def string_shorten(text, *, length):
    if len(text) > length:
        length_half = length // 2
        length_str = "<%d>" % len(text)
        text = "%s ... %s %s" % (text[:length_half], text[-length_half:], length_str)
    return text


# ----------------------------------------------------------------------------
# MixIn's
#
class TestFilesMixIn:

    @staticmethod
    def create_file_array_cow(
            iterator,
            stride, chunk_count):
        bs = array_cow.BLI_array_store_create(
                stride=stride, chunk_count=chunk_count)
        state_parent = None

        states = []

        for filename in iterator:
            with open(filename, 'rb') as f:
                data = f.read()
            state_parent = array_cow.BLI_array_store_state_add(bs, data, state_parent)
            states.append((filename, state_parent))
        return bs, states


class TestDataMixIn:

    def data_test(
            self,
            data_list,
            *,
            stride, chunk_count):

        data_list = tuple(data_list)
        bs, states = array_cow_from_bytes(
                data_list,
                stride=stride,
                chunk_count=chunk_count)
        for state, data in zip(states, data_list):
            data_test = array_cow.BLI_array_store_state_date_get(state)
            # print(data_test, data)
            self.assertEqual(data, data_test)

        data_list_repr = string_shorten(repr(data_list), length=72)
        print(repr_stats(bs), data_list_repr)

        # caller may optionally want to do further tests
        return bs, states

    @staticmethod
    def create_random_from_seed(
            *,
            seed,
            items,
            item_min=1, item_max=128,
            stride_min=1, stride_max=128,
            chunk_min=1, chunk_max=512
            ):

        import random

        if 0:
            # for true random (not reproducible).
            # seed = int(seed + random.random() * 1000)
            # print("seed is:", seed)
            import sys
            sys.stdout.flush()

        rng = random.Random(seed)
        kw = dict(
            stride=rng.randint(stride_min, stride_max),
            chunk_count=rng.randint(chunk_min, chunk_max),
            )

        data_list = []

        '''
        def urandom_from_random(length):
            return bytes([rng.randint(0, 255) for i in range(length)])
        '''
        # Limits size is 256mb, acceptable for our use.
        def urandom_from_random(length):
            if length == 0:
                return b''
            import sys
            integer = rng.getrandbits(length * 8)
            result = integer.to_bytes(length, sys.byteorder)
            return result

        for i in range(items):
            length = (rng.randint(item_min, item_max) // kw["stride"]) * kw["stride"]
            data_list.append(urandom_from_random(length))

        return kw, data_list


# ----------------------------------------------------------------------------
# TestClasses

class TestSourceCode(unittest.TestCase, TestFilesMixIn):

    # total number of files to read over
    # gives significant slowdown, but nice for comprehensive test.
    if 1:
        _files = 629
    else:
        _files = 10

    def test_c_source(self):

        def file_iter():
            basedir = os.path.join("test_data", "c_code") + os.sep
            for i in range(self._files):
                filename = "%s%04d.c" % (basedir, i)
                yield filename

        bs, states = self.create_file_array_cow(file_iter(), stride=1, chunk_count=128)

        for filename, state in states:
            with open(filename, 'rb') as f:
                data = f.read()
            data_test = array_cow.BLI_array_store_state_date_get(state)

            if 0:
                # to preview output and see whats different
                with open("/tmp/good.txt", 'wb') as f:
                    f.write(data)
                with open("/tmp/bad.txt", 'wb') as f:
                    f.write(data_test)

            self.assertEqual(data, data_test)
        print(repr_stats(bs))

    def test_data(self):

        def file_iter():
            basedir = os.path.join("test_data", "xz_data") + os.sep
            # for i in range(50):
            for i in range(TestSourceCode._files):
                filename = "%s%04d.c.xz" % (basedir, i)
                yield filename

        bs, states = self.create_file_array_cow(file_iter(), stride=1, chunk_count=128)

        for filename, state in states:
            with open(filename, 'rb') as f:
                data = f.read()
            data_test = array_cow.BLI_array_store_state_date_get(state)
            self.assertEqual(data, data_test)


class TestData_Random(unittest.TestCase, TestDataMixIn):

    def test_random_01(self):
        kw, data_list = self.create_random_from_seed(seed=1, items=1024)
        self.data_test(data_list, **kw)[0]

    def test_random_02(self):
        kw, data_list = self.create_random_from_seed(seed=10, items=1024)
        self.data_test(data_list, **kw)[0]

    def test_random_03(self):
        kw, data_list = self.create_random_from_seed(seed=100, items=1024)
        self.data_test(data_list, **kw)[0]

    def test_random_04(self):
        kw, data_list = self.create_random_from_seed(seed=1000, items=1024)
        self.data_test(data_list, **kw)[0]

    def test_random_05(self):
        kw, data_list = self.create_random_from_seed(seed=10000, items=1024)
        self.data_test(data_list, **kw)[0]

    def test_random_06(self):
        kw, data_list = self.create_random_from_seed(seed=100000, items=1024)
        self.data_test(data_list, **kw)[0]

    def test_random_07(self):
        kw, data_list = self.create_random_from_seed(seed=1000000, items=1024)
        self.data_test(data_list, **kw)[0]

    def test_random_08(self):
        kw, data_list = self.create_random_from_seed(seed=10000000, items=1024)
        self.data_test(data_list, **kw)[0]


class TestData_Bytes1(unittest.TestCase, TestDataMixIn):

    def test_bytes_nop(self):
        kw = dict(stride=1, chunk_count=32)
        self.data_test((), **kw)
        self.data_test((b'',), **kw)
        self.data_test((b'#',), **kw)
        self.data_test((b'#', b'#'), **kw)

    def test_bytes_single(self):
        data_list = (b"+", b"-")
        self.data_test(data_list, stride=1, chunk_count=32)

    def test_bytes_multi_nodupe(self):
        kw = dict(stride=1, chunk_count=32)
        data_list = (
            b"",
            b"test",
            b"",
            b"hello world",
            b"",
            b"world hello",
            b"",
            )
        bs = self.data_test(data_list, **kw)[0]
        size_real, size_full = array_cow_size_info(bs)
        self.assertEqual(size_real, size_full)

    def test_bytes_multi_match_decrease(self):
        kw = dict(stride=1, chunk_count=8)
        data_list = (
            b"A" * 64,
            b"A" * 32,
            b"A" * 16,
            b"A" * 8,
            )
        bs = self.data_test(data_list, **kw)[0]
        size_real, size_full = array_cow_size_info(bs)
        # larger then 8 since the first blocks don't de-duplicate
        self.assertEqual(size_real, 64)

    def test_bytes_multi_match_increase(self):
        kw = dict(stride=1, chunk_count=8)
        data_list = (
            b"A" * 8,
            b"A" * 16,
            b"A" * 32,
            b"A" * 64,
            )
        bs = self.data_test(data_list, **kw)[0]
        size_real, size_full = array_cow_size_info(bs)
        self.assertEqual(size_real, 8)

    def test_bytes_multi_dupe(self):
        kw = dict(stride=1, chunk_count=32)
        data_dupe = b"#" * 512
        data_list = (
            b"test",
            b"hello world",
            b"world hello",
            b"foobar",
            )
        data_list_dupe = (data_dupe + data for data in data_list)
        bs = self.data_test(data_list_dupe, **kw)[0]
        size_real, size_full = array_cow_size_info(bs)
        self.assertLess(size_real, size_full // 3)

        data_list_dupe = (data + data_dupe for data in data_list)
        bs = self.data_test(data_list_dupe, **kw)[0]
        size_real, size_full = array_cow_size_info(bs)
        self.assertLess(size_real, size_full // 3)

    def test_bytes_block_merge_test(self):
        """
        Gradual expanding to force block merging,
        which is infact very rare so this test case is needed to ensure that code-path runs.
        """
        kw = dict(stride=1, chunk_count=64)
        unique_text = (
                b"Lorem ipsum dolor sit amet, consectetur adipiscing elit. "
                b"Nam vitae massa vitae nunc venenatis suscipit.")
        data_list_prefix = [unique_text]
        data_list_suffix = [unique_text]

        import random
        rng = random.Random(42)
        for i in range(kw["chunk_count"] * 2):
            data_list_prefix.append(data_list_prefix[-1] + bytes([rng.randint(0, 255)]))
            data_list_suffix.append(bytes([rng.randint(0, 255)]) + data_list_suffix[-1])

        self.data_test(data_list_prefix, **kw)
        self.data_test(data_list_suffix, **kw)

        self.data_test(reversed(data_list_prefix), **kw)
        self.data_test(reversed(data_list_suffix), **kw)


class TestData_Bytes4(unittest.TestCase, TestDataMixIn):
    """
    Check the stride is working correctly to de-duplicate blocks
    """

    @staticmethod
    def data_vars():
        stride = 4
        chunk_count = 8
        kw = dict(stride=stride, chunk_count=chunk_count)

        chunk_size_total = stride * chunk_count

        data_a = b'A' * chunk_size_total
        data_b = b'B' * chunk_size_total
        data_c = b'C' * chunk_size_total
        data_d = b'D' * chunk_size_total

        data_z = b'Z' * chunk_size_total

        data_pad_min = b'#'
        data_pad_max = b'#' * (stride - 1)

        return (kw, data_a, data_b, data_c, data_d, data_z,
                data_pad_min, data_pad_max)

    def test_match(self):
        (kw, data_a, data_b, data_c, data_d, data_z, data_pad_min, data_pad_max) = self.data_vars()

        # match
        data_list = (
            data_a + data_b + data_c + data_d,
            data_a + data_b + data_c + data_d,
            )
        bs, states = self.data_test(data_list, **kw)
        size_real, size_full = array_cow_size_info(bs)

        self.assertEqual(size_real, size_full // 2)

    def test_match_reverse(self):
        (kw, data_a, data_b, data_c, data_d, data_z, data_pad_min, data_pad_max) = self.data_vars()

        # reverse chunks, add non-duplicate at end
        data_list = (
            data_a + data_b + data_c + data_d,
            data_d + data_c + data_b + data_a + data_z,
            )
        bs, states = self.data_test(data_list, **kw)
        size_real, size_full = array_cow_size_info(bs)

        self.assertEqual(size_real, (size_full - len(data_z)) // 2 + len(data_z))

    def test_match_reverse_except_1(self):
        (kw, data_a, data_b, data_c, data_d, data_z, data_pad_min, data_pad_max) = self.data_vars()

        # reverse chunks, add non-duplicate at end
        data_list = (
            data_a + data_b + data_c + data_d,
            data_z + data_d + data_c + data_b + data_a,
            )
        bs, states = self.data_test(data_list, **kw)
        size_real, size_full = array_cow_size_info(bs)

        self.assertEqual(size_real, (size_full - len(data_z)) // 2 + len(data_z))

    def test_match_stride_align_mismatch(self):
        (kw, data_a, data_b, data_c, data_d, data_z, data_pad_min, data_pad_max) = self.data_vars()

        # non-match, offset data from the stride, so we get 0% match
        data_list = (
            data_pad_min + data_a + data_b + data_c + data_d + data_pad_max,
            data_pad_max + data_a + data_b + data_c + data_d + data_pad_min,
            )
        bs, states = self.data_test(data_list, **kw)
        size_real, size_full = array_cow_size_info(bs)

        self.assertEqual(size_real, size_full)


if __name__ == '__main__':
    unittest.main(exit=False)
