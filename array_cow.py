from collections import namedtuple

__all__ = (
    "BLI_array_store_create",
    "BLI_array_store_destroy",

    "BLI_array_store_state_add",
    "BLI_array_store_state_remove",

    "BLI_array_store_state_size_get",
    "BLI_array_store_state_date_get",

    # info
    "BLI_array_store_state_size_real_calc",
    "BLI_array_store_state_size_expanded_calc"
    )


# -------
# Defines

# Scan first chunks (happy path when beginning of the array matches).
# When the array is a perfect match, we can re-use the entire list.
USE_FASTPATH_CHUNKS_FIRST = True

# Scan last chunks (happy path when end of the array matches).
# When the end of the array matches, we can quickly add these chunks.
# note that we will add contiguous matching chunks
# so this isn't as useful as USE_FASTPATH_CHUNKS_FIRST,
# however it avoids adding matching chunks into the lookup table,
# so creating the lookup table won't be as expensive.
USE_FASTPATH_CHUNKS_LAST = (True and USE_FASTPATH_CHUNKS_FIRST)

# For arrays of matching length, test that *enough* of the chunks are aligned,
# and simply step over both arrays, using matching chunks.
# This avoids overhead of using a lookup table for cases when we can assume they're mostly aligned.
USE_ALIGN_CHUNKS_TEST = True

# Search for non-aligned chunks
USE_HASH_TABLE = True

# Accumulate hashes from right to left so we can create a hash for the chunk-start.
# This serves to increase uniqueness and will help when there is many values which are the same.
USE_HASH_TABLE_ACCUMULATE = (True and USE_HASH_TABLE)

if USE_HASH_TABLE_ACCUMULATE:
    # Number of times to propagate hashes back.
    # Effectively a 'triangle-number'.
    # so 4 -> 7, 5 -> 10, 6 -> 15... etc.
    BCHUNK_HASH_TABLE_ACCUMULATE_STEPS = 4
else:
    # How many items to hash (multiplied by stride)
    BCHUNK_HASH_LEN = 4

# Calculate the key once and reuse it
USE_HASH_TABLE_KEY_CACHE = True

HASH_TABLE_KEY_UNSET = -1
HASH_TABLE_KEY_FALLBACK = -2


# How much larger the table is then the total number of chunks.
BCHUNK_HASH_TABLE_MUL = 3

# Merge small chunks:
#
# Using this means chunks below a threshold will be merged together.
# Even though short term this uses more memory,
# long term the overhead of maintaining many small chunks is reduced.
# This is defined by setting the minimum chunk size (as a fraction of the regular chunk size).
USE_MERGE_CHUNKS = True

if USE_MERGE_CHUNKS:
    # Merge chunks smaller then: (chunk_size // BCHUNK_MIN_SIZE_DIV)
    BCHUNK_SIZE_MIN_DIV = 8
    # Disallow chunks bigger then the regular chunk size scaled by this value
    # note: must be at least 2!
    # however, this code runs wont run in tests unless its ~1.1 ugh.
    # so lower only to check splitting works.
    BCHUNK_SIZE_MAX_MUL = 2


# ----------------------------------------------------------------------------
# Double Linked List API

class LinkNode:
    __slots__ = (
        "next",
        "prev",
        "link",
        )

    def __init__(self):
        self.next = None
        self.prev = None
        self.link = None


class ListBase:
    __slots__ = (
        "first",
        "last",
        )

    def __init__(self):
        self.first = None
        self.last = None


def listbase_append(ls, item):
    node = LinkNode()
    node.link = item

    if ls.first is None:
        assert(ls.last is None)
        ls.first = node
    else:
        node.prev = ls.last
        ls.last.next = node
    ls.last = node


def listbase_link_iter(lb):
    ref = lb.first
    while ref is not None:
        yield ref.link
        ref = ref.next

# End Linked List
# ---------------


# main storage for all states
#
# Since this may store multiple root nodes,
# its possible to store many trees of data
# as long as their stride & chunk size is compatible.
BArrayStore = namedtuple(
        "BArrayStore", (
        # static
        "info",       # int
        # dynamic

        # States may be in any order (logic should never depend on state order).
        "states"          # list
        ))

# A single instance of an array.
#
# This is how external API's hold a reference to an in-memory state,
# although the struct is private.
BArrayInfo = namedtuple(
        "BArrayInfo", (
        "chunk_stride",          # int
        "chunk_count",     # int

        # precalc
        "chunk_byte_size",  # int
        "chunk_byte_size_min",  # int
        "chunk_byte_size_max",  # int
        ) + ((
        "accum_steps",
        "accum_read_ahead_bytes",
        ) if USE_HASH_TABLE_ACCUMULATE else ()),
        )


# a single instance of an array
BArrayState = namedtuple(
        "BArrayState", (

        "chunk_list",  # BChunkList
        ))


class BChunkList:
    __slots__ = (
        "chunk_refs",      # ListBase
        "chunk_refs_len",  # int
        "total_size",  # int
        "users",       # int
        )
    # bchunk_list_new instead of __init__


class BChunk:
    __slots__ = (
        "data",   # bytes
        "users",  # int
        ) + (("key",) if USE_HASH_TABLE_KEY_CACHE else ())
    # bchunk_new instead of __init__


# ----------------------------------------------------------------------------
# Internal chunk API


# bchunk
#
def bchunk_new(
        *,
        data: bytes
        ) -> BChunk:
    chunk = BChunk()
    assert(isinstance(data, bytes))
    assert(len(data))
    chunk.data = data
    chunk.users = 0
    if USE_HASH_TABLE_KEY_CACHE:
        chunk.key = HASH_TABLE_KEY_UNSET
    return chunk


bchunk_new_copydata = bchunk_new


def bchunk_decref(chunk):
    chunk.users -= 1
    if chunk.users == 0:
        del chunk.data, chunk.users


def bchunk_data_compare(
        chunk: BChunk,
        data_base: memoryview,
        data_base_len: int,
        offset
        ) -> bool:
    # # Works but too slow:
    # return data_base[offset:data_base_len].startswith(sub_data)
    if offset + len(chunk.data) <= data_base_len:
        return (data_base[offset:offset + len(chunk.data)] == chunk.data)
    else:
        return False


# bchunk_list
#
def bchunk_list_new(*, total_size):
    chunk_list = BChunkList()
    chunk_list.chunk_refs = ListBase()
    chunk_list.chunk_refs_len = 0
    chunk_list.total_size = total_size
    chunk_list.users = 0
    return chunk_list


if USE_MERGE_CHUNKS:
    def bchunk_list_ensure_min_size_last(
            info: BArrayInfo,
            chunk_list: BChunkList,
            ):

        cref = chunk_list.chunk_refs.last
        if cref and cref.prev:
            chunk_curr = cref.link
            chunk_prev = cref.prev.link
            if min(len(chunk_prev.data), len(chunk_curr.data)) < info.chunk_byte_size_min:
                data_merge_len = len(chunk_curr.data) + len(chunk_prev.data)

                # we could pass, but no need
                if data_merge_len <= info.chunk_byte_size_max:

                    # we have enough space to merge

                    # remove last from linklist
                    cref.prev.next = None
                    chunk_list.chunk_refs.last = cref.prev
                    chunk_list.chunk_refs_len -= 1

                    cref.prev.link = bchunk_new(data=chunk_prev.data + chunk_curr.data)
                    cref.prev.link.users += 1

                else:
                    # If we always merge small slices, we should _almost_ never end up having very large chunks.
                    # Gradual expanding on contracting will cause this.
                    #
                    # if we do, the code below works (test by setting 'BCHUNK_SIZE_MAX_MUL = 1.2')

                    # merge and split
                    data = chunk_prev.data + chunk_curr.data

                    # keep chunk on the left hand side a regular size
                    split = info.chunk_byte_size

                    cref.prev.link = bchunk_new_copydata(data=data[:split])
                    cref.prev.link.users += 1

                    cref.link = bchunk_new_copydata(data=data[split:])
                    cref.link.users += 1

                bchunk_decref(chunk_curr)
                bchunk_decref(chunk_prev)
                # free zero users


def bchunk_list_calc_trim_len(
        info: BArrayInfo,
        data_len: int):

    """
    Split length into 2 values
    :return: a pair of lengths,
       data_trim_len: Length which is aligned to the #BArrayInfo.chunk_byte_size.
       data_last_chunk_len: The remaining bytes.

    .. note::

       This function ensures the size of \a r_data_last_chunk_len
       is larger than #BArrayInfo.chunk_byte_size_min.
    """

    data_last_chunk_len = 0
    data_trim_len = data_len

    if USE_MERGE_CHUNKS:
        # avoid creating too-small chunks
        # more efficient then merging after */
        if data_len > info.chunk_byte_size:
            data_last_chunk_len = (data_trim_len % info.chunk_byte_size)
            data_trim_len = data_trim_len - data_last_chunk_len
            if data_last_chunk_len:
                if data_last_chunk_len < info.chunk_byte_size_min:
                    # may be zero and thats OK
                    data_trim_len -= info.chunk_byte_size
                    data_last_chunk_len += info.chunk_byte_size
        else:
            data_trim_len = 0
            data_last_chunk_len = data_len

        assert((data_trim_len == 0) or (data_trim_len >= info.chunk_byte_size))
    else:
        data_last_chunk_len = (data_trim_len % info.chunk_byte_size)
        data_trim_len = data_trim_len - data_last_chunk_len

    assert(data_trim_len + data_last_chunk_len == data_len)

    return data_trim_len, data_last_chunk_len


def bchunk_list_append_only(
        info: BArrayInfo,
        chunk_list: BChunkList,
        chunk: BChunk,
        ):
    """
    Append and don't manage merging small chunks.
    """
    listbase_append(chunk_list.chunk_refs, chunk)
    chunk_list.chunk_refs_len += 1
    chunk.users += 1


def bchunk_list_append_data(
        info: BArrayInfo,
        chunk_list: BChunkList,
        data: memoryview,
        ):
    """
    note This is for writing single chunks,
    use bchunk_list_append_data_n when writing large blocks of memory into many chunks.
    """

    assert(len(data) != 0)
    assert(isinstance(data, memoryview))

    if USE_MERGE_CHUNKS:
        if chunk_list.chunk_refs.last:
            cref = chunk_list.chunk_refs.last
            chunk_prev = cref.link

            if min(len(chunk_prev.data), len(data)) < info.chunk_byte_size_min:
                if cref.link.users == 1:
                    cref.link.data = cref.link.data + data
                else:
                    cref.link = bchunk_new(data=chunk_prev.data + data)
                    cref.link.users += 1
                    bchunk_decref(chunk_prev)
                return

    if isinstance(data, memoryview):
        data = data.tobytes()

    chunk = bchunk_new_copydata(data=data)

    bchunk_list_append_only(info, chunk_list, chunk)

    # # don't run this, instead preemptively avoid creating a chunk only to merge it (above).
    # if USE_MERGE_CHUNKS:
    #     bchunk_list_ensure_min_size_last(info, chunk_list)


def bchunk_list_append_data_n(
        info: BArrayInfo,
        chunk_list: BChunkList,
        data: memoryview):

    # Similar to #bchunk_list_append_data, but handle multiple chunks.
    # Use for adding arrays of arbitrary sized memory at once.
    #
    # \note This function takes care not to perform redundant chunk-merging checks,
    # so we can write successive fixed size chunks quickly.

    data_trim_len, data_last_chunk_len = bchunk_list_calc_trim_len(info, len(data))
    assert(data_trim_len + data_last_chunk_len == len(data))

    if data_trim_len != 0:
        i = info.chunk_byte_size
        bchunk_list_append_data(info, chunk_list, data[:i])
        i_prev = i
        del i

        while i_prev != data_trim_len:
            i = i_prev + info.chunk_byte_size
            chunk = bchunk_new_copydata(data=data[i_prev:i].tobytes())
            bchunk_list_append_only(info, chunk_list, chunk)
            i_prev = i

        if data_last_chunk_len:
            chunk = bchunk_new_copydata(data=data[i_prev:i_prev + data_last_chunk_len].tobytes())
            bchunk_list_append_only(info, chunk_list, chunk)
            # i_prev = data_len;  /* UNUSED */
    else:
        # if we didn't write any chunks previously,
        # we may need to merge with the last.
        if data_last_chunk_len:
            bchunk_list_append_data(info, chunk_list, data[:data_last_chunk_len])
            # i_prev = data_len;  /* UNUSED */

    if USE_MERGE_CHUNKS:
        if len(data) > info.chunk_byte_size:
            assert(len(chunk_list.chunk_refs.last.link.data) >= info.chunk_byte_size_min)


def bchunk_list_append(
        info: BArrayInfo,
        chunk_list: BChunkList,
        chunk: BChunk,
        ):
    bchunk_list_append_only(info, chunk_list, chunk)

    if USE_MERGE_CHUNKS:
        bchunk_list_ensure_min_size_last(info, chunk_list)

    # if len(chunk.data) < info.chunk_byte_size_min:
    #     print(len(chunk.data), info.chunk_byte_size_min)


def bchunk_list_fill_from_array(
        info: BArrayInfo,
        chunk_list: BChunkList,
        data: memoryview,
        ) -> BChunkList:

    assert(chunk_list.chunk_refs.first is None)

    data_trim_len, data_last_chunk_len = bchunk_list_calc_trim_len(info, len(data))
    assert(data_trim_len + data_last_chunk_len == len(data))

    i_prev = 0
    while i_prev != data_trim_len:
        i = i_prev + info.chunk_byte_size
        data_slice = data[i_prev:i].tobytes()
        chunk = bchunk_new_copydata(data=data_slice)
        bchunk_list_append_only(info, chunk_list, chunk)
        i_prev = i

    if data_last_chunk_len:
        data_slice = data[i_prev:len(data)].tobytes()
        chunk = bchunk_new_copydata(data=data_slice)
        bchunk_list_append_only(info, chunk_list, chunk)

    if USE_MERGE_CHUNKS:
        if len(data) > info.chunk_byte_size:
            assert(len(chunk_list.chunk_refs.last.link.data) >= info.chunk_byte_size_min)

    # no need, we ensure small chunks aren't written above
    # # if USE_MERGE_CHUNKS:
    # #     bchunk_list_ensure_min_size_last(info, chunk_list)


# ----------------------------------------------------------------------------
# Internal Table Lookup Functions

if USE_HASH_TABLE_ACCUMULATE:
    def hash_array_from_data_ex(
            info, data_slice, data_slice_len,
            hash_array, hash_array_offset,
            ):
        i_step = 0
        for i in range(hash_array_offset, hash_array_offset + len(hash_array)):
            i_next = i_step + info.chunk_stride
            # XXX, tobytes() call here is only needed for pypy3, CPython works without it
            hash_array[i] = hash(data_slice[i_step:i_next].tobytes())
            i_step = i_next
        assert(i_step <= data_slice_len)

    def hash_array_from_data(info, data_slice, data_slice_len):
        hash_array = [0] * (data_slice_len // info.chunk_stride)
        hash_array_from_data_ex(info, data_slice, data_slice_len, hash_array, 0)
        return hash_array

    def hash_array_from_cref(info, cref, data_len):
        """
        Similar to hash_array_from_data,
        but able to step into the next chunk if we run-out of data.
        """

        '''
        if data_len <= len(cref.link.data):
            return hash_array_from_chunk(info, cref.link, data_len)
        '''

        hash_array = [0] * (data_len // info.chunk_stride)
        i = 0
        while True:
            i_next = len(hash_array) - i
            data_trim_len = i_next * info.chunk_stride
            if data_trim_len > len(cref.link.data):
                data_trim_len = len(cref.link.data)
                i_next = data_trim_len // info.chunk_stride
            assert(data_trim_len <= len(cref.link.data))
            hash_array_from_data_ex(info, memoryview(cref.link.data), data_trim_len, hash_array, i)
            i += i_next
            cref = cref.next

            if not ((i < len(hash_array)) and (cref is not None)):
                break

        assert(i == len(hash_array))
        return hash_array

    def hash_accum(hash_array, iter_steps):
        assert(iter_steps < len(hash_array))
        if not (iter_steps < len(hash_array)):
            # while this shouldn't happen, avoid crashing
            iter_steps = len(hash_array)
        hash_array_search_len = len(hash_array) - iter_steps
        while iter_steps > 0:
            hash_offset = iter_steps
            for i in range(hash_array_search_len):
                hash_array[i] += (hash_array[i + hash_offset]) * ((hash_array[i] & 0xff) + 1)
            iter_steps -= 1

    def hash_accum_single(hash_array, iter_steps):
        """
        When we only need a single value,
        we can avoid accumulating the tail of the array a little, each iteration.
        This saves a bit of CPU time.
        """
        assert(iter_steps < len(hash_array))
        if not (iter_steps < len(hash_array)):
            # while this shouldn't happen, avoid crashing
            iter_steps = len(hash_array)

        # We can increase this value each step to avoid accumulating quite as much
        # while getting the same results as hash_accum
        iter_steps_sub = iter_steps

        while iter_steps > 0:
            hash_offset = iter_steps
            hash_search = len(hash_array) - iter_steps_sub
            for i in range(hash_search):
                hash_array[i] += (hash_array[i + hash_offset]) * ((hash_array[i] & 0xff) + 1)
            iter_steps -= 1
            iter_steps_sub += iter_steps

    def key_from_chunk_ref(info, cref):
        # in C, will fill in a reusable array
        chunk = cref.link
        if info.accum_read_ahead_bytes <= len(chunk.data):
            if USE_HASH_TABLE_KEY_CACHE:
                key = chunk.key
                if key != HASH_TABLE_KEY_UNSET:
                    # Using key cache!
                    # avoids calculating every time
                    pass
                else:
                    hash_array = hash_array_from_cref(info, cref, info.accum_read_ahead_bytes)
                    hash_accum_single(hash_array, info.accum_steps)
                    key = hash_array[0]

                    # cache the key
                    if key == HASH_TABLE_KEY_UNSET:
                        key = HASH_TABLE_KEY_FALLBACK
                    chunk.key = key
            else:
                hash_array = hash_array_from_cref(info, cref, info.accum_read_ahead_bytes)
                hash_accum_single(hash_array, info.accum_steps)
                key = hash_array[0]
            return key
        else:
            # corner case - we're too small, calculate the key each time.
            hash_array = hash_array_from_cref(info, cref, info.accum_read_ahead_bytes)
            hash_accum_single(hash_array, info.accum_steps)
            key = hash_array[0]

            if USE_HASH_TABLE_KEY_CACHE:
                # for consistent results with USE_HASH_TABLE_KEY_CACHE
                if key == HASH_TABLE_KEY_UNSET:
                    key = HASH_TABLE_KEY_FALLBACK
            return key
        assert(0)

    def table_lookup(info, table, i_table_start, data, data_len, offset, table_hash_array):
        size_left = data_len - offset
        key = table_hash_array[((offset - i_table_start) // info.chunk_stride)]
        key_index = key % len(table)
        for i, cref in enumerate(table[key_index]):
            chunk_test = cref.link
            if (not USE_HASH_TABLE_KEY_CACHE) or (key == chunk_test.key):
                if len(chunk_test.data) <= size_left:
                    if bchunk_data_compare(chunk_test, data, data_len, offset):
                        # avoid finding again, is this useful???
                        # del table[key_index][i]
                        # print("found")
                        return cref
        return None


else:
    # NON USE_HASH_TABLE_ACCUMULATE code (simply hash each chunk)

    def key_from_chunk_ref(info, cref):
        data_hash_len = BCHUNK_HASH_LEN * info.chunk_stride

        chunk = cref.link
        if USE_HASH_TABLE_KEY_CACHE:
            key = chunk.key
            if key != HASH_TABLE_KEY_UNSET:
                # Using key cache!
                # avoids calculating every time
                pass
            else:
                # cache the key
                key = hash(chunk.data[0:data_hash_len])
                if key == HASH_TABLE_KEY_UNSET:
                    key = HASH_TABLE_KEY_FALLBACK
                chunk.key = key
        else:
            key = hash(chunk.data[0:data_hash_len])

        return key

    def table_lookup(info, table, i_table_start, data, data_len, offset, _):
        data_hash_len = BCHUNK_HASH_LEN * info.chunk_stride

        size_left = data_len - offset
        key = hash(data[offset:offset + min(data_hash_len, size_left)])
        key_index = key % len(table)
        for i, cref in enumerate(table[key_index]):
            chunk_test = cref.link
            if (not USE_HASH_TABLE_KEY_CACHE) or (key == chunk_test.key):
                if len(chunk_test.data) <= size_left:
                    if bchunk_data_compare(chunk_test, data, data_len, offset):
                        # avoid finding again, is this useful???
                        # del table[key_index][i]
                        return cref
        return None

# End Table Lookup
# ----------------


def bchunk_list_from_data_merge(
        info: BArrayInfo,
        data: bytes,
        chunk_list_reference: BChunkList,
        ) -> None:
    '''
    data: Data to store in the returned value.
    data_len_original: Length of data in bytes.
    chunk_list_reference: Reuse this list or chunks within it, don't modify its content.
    '''

    # ------------------------------------------------------------------------
    # Fast-Path for exact match
    #
    # Check for exact match, if so, return the current list with an extra user

    full_match = False
    cref_match_first = None
    chunk_list_reference_skip_len = 0
    chunk_list_reference_skip_bytes = 0

    if USE_FASTPATH_CHUNKS_FIRST:
        full_match = True

        cref = chunk_list_reference.chunk_refs.first
        i_prev = 0
        while i_prev < len(data):
            if cref is not None and bchunk_data_compare(cref.link, data, len(data), i_prev):
                cref_match_first = cref
                chunk_list_reference_skip_len += 1
                chunk_list_reference_skip_bytes += len(cref.link.data)
                i_prev += len(cref.link.data)
                cref = cref.next
            else:
                full_match = False
                break
        del cref

    # no change in the array!
    if full_match:
        if chunk_list_reference.total_size == len(data):
            return chunk_list_reference

    # End Fast-Path (first)
    # ---------------------

    # Copy until we have a mismatch
    chunk_list = bchunk_list_new(total_size=len(data))
    if cref_match_first is not None:
        chunk_size_step = 0
        cref = chunk_list_reference.chunk_refs.first
        while True:
            chunk = cref.link
            chunk_size_step += len(chunk.data)
            bchunk_list_append_only(info, chunk_list, chunk)
            del chunk
            if cref is cref_match_first:
                break
            else:
                cref = cref.next
        # happens when bytes are removed from the end of the array
        if chunk_size_step == len(data):
            return chunk_list

        i_prev = chunk_size_step
        del chunk_size_step, cref
    else:
        i_prev = 0

    # ------------------------------------------------------------------------
    # Fast-Path for end chunks
    #
    # Check for trailing chunks

    # # In this case use 'chunk_list_reference_last' to define the last index
    # index_match_last = -1

    # warning, from now on don't use len(data)
    # since we want to ignore chunks already matched
    data_len = len(data)
    chunk_list_reference_last = None

    if chunk_list_reference.chunk_refs.last is None:
        assert(chunk_list_reference.chunk_refs.first is None)
    elif USE_FASTPATH_CHUNKS_LAST:
        cref = chunk_list_reference.chunk_refs.last
        while ((cref.prev is not None) and
               (cref is not cref_match_first) and
               (len(cref.link.data) <= data_len - i_prev)):
            chunk_test = cref.link
            offset = data_len - len(chunk_test.data)
            # ensure we don't rewind too far
            assert(offset >= i_prev)
            if bchunk_data_compare(chunk_test, data, data_len, offset):
                data_len = offset
                chunk_list_reference_last = cref
                chunk_list_reference_skip_len += 1
                chunk_list_reference_skip_bytes += len(cref.link.data)
                cref = cref.prev
                del chunk_test, offset
            else:
                del chunk_test, offset
                break
        del cref

    # print(list(sorted(locals().keys())))

    # End Fast-Path (last)
    # --------------------

    # ------------------------------------------------------------------------
    # Check for aligned chunks
    #
    # This saves a lot of searching, so use simple heuristics to detect aligned arrays.
    # (may need to tweak exact method).

    use_aligned = False

    if USE_ALIGN_CHUNKS_TEST:
        if chunk_list.total_size == chunk_list_reference.total_size:
            # if we're already a quarter aligned
            if data_len - i_prev <= chunk_list.total_size // 4:
                use_aligned = True
            else:
                # TODO, walk over chunks and check if some arbitrary amount align
                pass

    # End Aligned Chunks Case
    # -----------------------

    if use_aligned:

        # Copy matching chunks, creates using the same 'layout' as the reference
        cref = cref_match_first.next if cref_match_first else chunk_list_reference.chunk_refs.first
        while i_prev != data_len:
            i = i_prev + len(cref.link.data)
            assert(i != i_prev)

            if ((cref is not chunk_list_reference_last) and
                    bchunk_data_compare(cref.link, data, data_len, i_prev)):
                bchunk_list_append(info, chunk_list, cref.link)
            else:
                bchunk_list_append_data(info, chunk_list, data[i_prev:i])

            cref = cref.next

            i_prev = i
            # assert(i_prev == sum(len(_.data) for _ in chunk_list.chunk_refs))
            del i

    elif ((data_len - i_prev >= info.chunk_byte_size) and
          (chunk_list_reference.chunk_refs_len >= chunk_list_reference_skip_len) and
          (chunk_list_reference.chunk_refs.first is not None)):

        # --------------------------------------------------------------------
        # Non-Aligned Chunk De-Duplication

        # only create a table if we have at least one chunk to search
        # otherwise just make a new one.
        #
        # Support re-arranged chunks

        if USE_HASH_TABLE_ACCUMULATE:

            i_table_start = i_prev
            table_hash_array = hash_array_from_data(info, data[i_prev:], data_len - i_prev)

            hash_accum(table_hash_array, info.accum_steps)
        else:
            table_hash_array = None
            i_table_start = None

        # tabke_make - inline
        # include one matching chunk, to allow for repeating values
        chunk_list_reference_bytes_remaining = (
                chunk_list_reference.total_size - chunk_list_reference_skip_bytes)

        chunk_list_reference_remaining_len = (
                (chunk_list_reference.chunk_refs_len - chunk_list_reference_skip_len) + 1)

        table_len = chunk_list_reference_remaining_len * BCHUNK_HASH_TABLE_MUL
        table = [[] for i in range(table_len)]

        if cref_match_first is not None:
            cref = cref_match_first
            chunk_list_reference_bytes_remaining += len(cref.link.data)
        else:
            cref = chunk_list_reference.chunk_refs.first

        while ((cref is not chunk_list_reference_last) and
               (chunk_list_reference_bytes_remaining >= info.accum_read_ahead_bytes)):

            key = key_from_chunk_ref(info, cref)
            key_index = key % len(table)
            table[key_index].append(cref)
            chunk_list_reference_bytes_remaining -= len(cref.link.data)
            cref = cref.next
        # done making the table

        # assert(i_prev == sum(len(_.data) for _ in chunk_list.chunk_refs))

        i = i_prev
        assert(i_prev <= data_len)
        while i < data_len:
            # Assumes exiting chunk isnt a match!

            cref_found = table_lookup(info, table, i_table_start, data, data_len, i, table_hash_array)
            if cref_found is not None:
                assert(i < data_len)
                # print("A", i - i_prev)
                if i != i_prev:
                    bchunk_list_append_data_n(info, chunk_list, data[i_prev:i])
                    i_prev = i

                # now add the reference chunk
                chunk_found = cref_found.link
                i += len(chunk_found.data)
                bchunk_list_append(info, chunk_list, chunk_found)
                del chunk_found
                i_prev = i
                assert(i_prev <= data_len)

                # its likely that the next chunk in the list will be a match, so check it!
                while cref_found.next not in {None, chunk_list_reference_last}:
                    cref_found = cref_found.next
                    chunk_found = cref_found.link

                    if bchunk_data_compare(chunk_found, data, data_len, i_prev):
                        # may be useful to remove table data, assuming we dont have repeating memory
                        # where it would be useful to re-use chunks.
                        i += len(chunk_found.data)
                        bchunk_list_append(info, chunk_list, chunk_found)
                        del chunk_found
                        i_prev = i
                        assert(i_prev <= data_len)
                    else:
                        break

                del cref_found
            else:
                i = i + info.chunk_stride
        del i

        # End Table Lookup
        # ----------------

    # ------------------------------------------------------------------------
    # No Duplicates to copy, write new chunks
    #
    # Trailing chunks, no matches found in table lookup above.
    # Write all new data.
    assert(i_prev <= data_len)

    if i_prev != data_len:
        bchunk_list_append_data_n(info, chunk_list, data[i_prev:data_len])
        i_prev = data_len

    if USE_FASTPATH_CHUNKS_LAST:
        if chunk_list_reference_last is not None:
            # write chunk_list_reference_last since it hasn't been written yet
            cref = chunk_list_reference_last

            if USE_MERGE_CHUNKS:
                chunk = cref.link
                i_prev += len(chunk.data)
                bchunk_list_append(info, chunk_list, chunk)
                cref = cref.next

            while cref is not None:
                chunk = cref.link
                # print(data[i_prev:i_prev + len(chunk.data)])
                # print(chunk.data)
                # assert(bchunk_data_compare(chunk, data, len(data), i_prev))
                i_prev += len(chunk.data)
                # use simple since we assume the references chunks have already been sized correctly.
                bchunk_list_append_only(info, chunk_list, chunk)
                del chunk
                cref = cref.next

            if USE_MERGE_CHUNKS:
                bchunk_list_ensure_min_size_last(info, chunk_list)

    if (i_prev != len(data)):
        print(i_prev, len(data))
    assert(i_prev == len(data))

    return chunk_list


# end private API

def BLI_array_store_create(
        *,
        stride: int,
        chunk_count: int
        ) -> BArrayStore:

    info_kw = {}

    if USE_HASH_TABLE_ACCUMULATE:
        accum_steps = BCHUNK_HASH_TABLE_ACCUMULATE_STEPS - 1
        info_kw["accum_steps"] = accum_steps
        # Triangle number, identifying now much read-ahead we need:
        # https://en.wikipedia.org/wiki/Triangular_number (+ 1)
        info_kw["accum_read_ahead_bytes"] = ((((accum_steps * (accum_steps + 1))) // 2) + 1) * stride

    if USE_MERGE_CHUNKS:
        chunk_byte_size_min = chunk_count // BCHUNK_SIZE_MIN_DIV
        chunk_byte_size_max = chunk_count * BCHUNK_SIZE_MAX_MUL
    else:
        chunk_byte_size_min = None

    info = BArrayInfo(
            chunk_stride=stride,
            chunk_count=chunk_count,

            chunk_byte_size=stride * chunk_count,
            chunk_byte_size_min=chunk_byte_size_min,
            chunk_byte_size_max=chunk_byte_size_max,

            **info_kw
            )

    return BArrayStore(
            info=info,
            states=[],
            )


def barray_free(
        bd: BArrayStore,
        ) -> None:
    bd.slots.clear()


def _barray_validate_users(bd):
    """
    Validate the user count (quite slow! only for debugging).
    """
    lists = {}

    for state in bd.states:
        try:
            lists[state.chunk_list] += 1
        except KeyError:
            lists[state.chunk_list] = 1

    users = {}
    for chunk_list in lists:
        for chunk in listbase_link_iter(chunk_list.chunk_refs):
            try:
                users[chunk] += 1
            except KeyError:
                users[chunk] = 1

    for chunk_list, users_real in lists.items():
        assert(chunk_list.users == users_real)

    for chunk, users_real in users.items():
        assert(chunk.users == users_real)


if USE_MERGE_CHUNKS:
    def barray_validate_sizes(info, state):
        if state.chunk_list.total_size <= info.chunk_byte_size_min:
            return

        for chunk in listbase_link_iter(state.chunk_list.chunk_refs):
            assert(len(chunk.data) >= info.chunk_byte_size_min)


def BLI_array_store_state_add(
        bd: BArrayStore,
        data: bytes,  # or memoryview
        state_parent: BArrayState,  # or None
        ) -> BArrayState:

    if not isinstance(data, memoryview):
        assert(isinstance(data, bytes))
        data = memoryview(data)
    assert(data.readonly)

    # ensure we're aligned to the stride
    if (len(data) % bd.info.chunk_stride):
        print(len(data), bd.info.chunk_stride, (len(data) % bd.info.chunk_stride))
    assert((len(data) % bd.info.chunk_stride) == 0)

    if state_parent:
        chunk_list = bchunk_list_from_data_merge(
                bd.info,
                data,
                # re-use reference chunks
                state_parent.chunk_list,
                )
    else:
        chunk_list = bchunk_list_new(total_size=len(data))
        bchunk_list_fill_from_array(
                bd.info,
                chunk_list,
                data,
                )
    chunk_list.users += 1

    state = BArrayState(
            chunk_list=chunk_list,
            )

    bd.states.append(state)

    if 0:
        _barray_validate_users(bd)

    if USE_MERGE_CHUNKS:
        barray_validate_sizes(bd.info, state)

    return state


def BLI_array_store_state_remove(
        bd: BArrayStore,
        state: BArrayState,
        ) -> None:

    for chunk in state.chunk_list:
        chunk.users -= 1
        if chunk.users == 0:
            # free chunk!
            chunk.data = None

    state.chunk_list.clear()
    state.states.clear()


def BLI_array_store_state_size_get(
        state: BArrayState,
        ) -> int:

    return state.chunk_list.total_size


def BLI_array_store_state_date_get(
        state: BArrayState,
        ) -> bytes:

    # C version will fill an array
    data_list = [chunk.data for chunk in listbase_link_iter(state.chunk_list.chunk_refs)]
    assert(len(data_list) == state.chunk_list.chunk_refs_len)
    return b"".join(data_list)


def BLI_array_store_state_size_real_calc(
        bd: BArrayStore,
        ) -> int:

    used = set()

    def accum(c):
        l = len(used)
        used.add(c)
        if len(used) != l:
            return len(c.data)
        return 0

    i = 0
    for state in bd.states:
        i += sum(accum(c) for c in listbase_link_iter(state.chunk_list.chunk_refs))
    return i


def BLI_array_store_state_size_expanded_calc(
        bd: BArrayStore,
        ) -> int:

    i = 0
    for state in bd.states:
        i += state.chunk_list.total_size
    return i

