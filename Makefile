# note: this isn't needed for building
# its just for some convenience targets.

ifndef PYTHON
	PYTHON:=pypy3
endif

# pep8 test
PY_FILES=$(shell find $(PWD)/ -type f -name '*.py' -not -path '*/\.*')
pep8: .FORCE
	- flake8 $(PY_FILES) --ignore=E501,E302,E123,E126,E128,E129,E124,E122 > pep8.log
	gvim --nofork -c "cfile pep8.log" -c "cope" -c "clast"
	rm pep8.log

test: .FORCE
	$(PYTHON) ./test.py

.FORCE:

