
#################
 Py Array (COW)
#################

Reference library for in-memory de-duplicating storage.

Characteristics

- Array based *(stores a multiple contiguous arrays)*.
- Uses a *stride* for more efficiently diffing arrays of ``structs``.
- Performs a quick check for matching chunks at the beginning/end of the array.
- Exact matches of chunks, or the entire list will re-use data.
- Chunks are hashed & cached for de-duplication to find a match when the array has non matching chunks mixed in.
- Small chunks are merged to avoid fragmentation.
- Fast-path for checking arrays which are the same size, where we can assume they're already aligned.
  Where different data just duplicates chunks instead of searching.
- A tree of history is supported (adding a new state just requires passing in the previous),
  a single state may have multiple states based on it.

TODO:

- Support hinting that the data isn't re-arranged, will speed up when we _know_ this is the case.
- Investigate different methods for looking up duplicate chunks,
  or even replace with some more limited search.
- Compression (may be nice to support fast in-memory compression).


Files
=====

- ``array_cow.py`` source code.
- ``test.py`` all test code.
- ``test_data/`` data for tests.
